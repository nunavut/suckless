/* catppuccin mocha theme */
static const char *background_color = "#1c1213";
static const char *border_color = "#3a2425";
static const char *font_color = "#a87569";
