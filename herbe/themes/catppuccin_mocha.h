/* catppuccin mocha theme */
static const char *background_color = "#1e1e2e";
static const char *border_color = "#313244";
static const char *font_color = "#cdd6f4";
