/* rebecca theme 
 * https://github.com/vic/base16-rebecca */
static const char *background_color = "#292a44";
static const char *border_color = "#b45bcf";
static const char *font_color = "#f1eff8";
