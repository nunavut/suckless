/* gruvbox (dark) theme */
static const char *background_color = "#1d2021";
static const char *border_color = "#1d2021";
static const char *font_color = "#d5c4a1";
