/* gruvbox (dark) theme */
static const char *background_color = "#282828";
static const char *border_color = "#282828";
static const char *font_color = "#d5c4a1";
