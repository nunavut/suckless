#include "themes/nord.h"
static const char *font_pattern = "JetBrainsMono Nerd Font:size=8";
static unsigned line_spacing = 14;
static unsigned int padding = 25;

static unsigned int width = 300;
static unsigned int border_size = 2;
static unsigned int pos_x = 30;
static unsigned int pos_y = 60;

enum corners { TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT };
enum corners corner = TOP_RIGHT;

static unsigned int duration = 3; /* in seconds */

#define DISMISS_BUTTON Button1
#define ACTION_BUTTON Button3
