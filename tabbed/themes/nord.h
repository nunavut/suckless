/* nord theme */
static const char* normbgcolor  = "#2E3440";
static const char* normfgcolor  = "#e5e9f0";
static const char* selbgcolor   = "#3B4252";
static const char* selfgcolor   = "#eceff4";
static const char* urgbgcolor   = "#bf616a";
static const char* urgfgcolor   = "#eceff4";
