/* rebecca theme 
 * https://github.com/vic/base16-rebecca */
static const char* normbgcolor  = "#3a2425";
static const char* normfgcolor  = "#a87569";
static const char* selbgcolor   = "#1c1213";
static const char* selfgcolor   = "#a87569";
static const char* urgbgcolor   = "#c24f57";
static const char* urgfgcolor   = "#1c1213";
