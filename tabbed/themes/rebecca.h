/* rebecca theme 
 * https://github.com/vic/base16-rebecca */
static const char* normbgcolor  = "#383a62";
static const char* normfgcolor  = "#a0a0c5";
static const char* selbgcolor   = "#b45bcf";
static const char* selfgcolor   = "#f1eff8";
static const char* urgbgcolor   = "#663399";
static const char* urgfgcolor   = "#f1eff8";
