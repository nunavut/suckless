static const char* normbgcolor  = "#1f292e";
static const char* normfgcolor  = "#7ea2b4";
static const char* selbgcolor   = "#257fad";
static const char* selfgcolor   = "#ebf8ff";
static const char* urgbgcolor   = "#7195a8";
static const char* urgfgcolor   = "#161b1d";
