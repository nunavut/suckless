/* gruvbox theme */
static const char* normbgcolor  = "#3c3836";
static const char* normfgcolor  = "#fbf1c7";
static const char* selbgcolor   = "#83a598";
static const char* selfgcolor   = "#1d2021";
static const char* urgbgcolor   = "#fbf1c7";
static const char* urgfgcolor   = "#1d2021";
