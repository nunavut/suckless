static const char* normbgcolor  = "#101010";
static const char* normfgcolor  = "#c1c1c1";
static const char* selbgcolor   = "#5f8787";
static const char* selfgcolor   = "#101010";
static const char* urgbgcolor   = "#444444";
static const char* urgfgcolor   = "#c1c1c1";
