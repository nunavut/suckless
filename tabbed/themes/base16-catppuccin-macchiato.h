static const char* normbgcolor  = "#24273a";
static const char* normfgcolor  = "#cad3f5";
static const char* selbgcolor   = "#363a4f";
static const char* selfgcolor   = "#b7bdf8";
static const char* urgbgcolor   = "#ed8796";
static const char* urgfgcolor   = "#1e2030";
