/* St Johns theme */
static const char* normbgcolor  = "#101111";
static const char* normfgcolor  = "#d5c1ab";
static const char* selbgcolor   = "#9c642a";
static const char* selfgcolor   = "#d5c1ab";
static const char* urgbgcolor   = "#1011111";
static const char* urgfgcolor   = "#9c642a";
