static const char* normbgcolor  = "#353b45";
static const char* normfgcolor  = "#abb2bf";
static const char* selbgcolor   = "#282c34";
static const char* selfgcolor   = "#c8ccd4";
static const char* urgbgcolor   = "#c8ccd4";
static const char* urgfgcolor   = "#353b45";
