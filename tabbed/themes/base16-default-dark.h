static const char* normbgcolor  = "#383838";
static const char* normfgcolor  = "#d8d8d8";
static const char* selbgcolor   = "#282828";
static const char* selfgcolor   = "#e8e8e8";
static const char* urgbgcolor   = "#e8e8e8";
static const char* urgfgcolor   = "#383838";
