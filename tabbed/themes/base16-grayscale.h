static const char* normbgcolor  = "#464646";
static const char* normfgcolor  = "#b9b9b9";
static const char* selbgcolor   = "#252525";
static const char* selfgcolor   = "#f7f7f7";
static const char* urgbgcolor   = "#747474";
static const char* urgfgcolor   = "#101010";
