/* blaquemagick theme */
static const char* normbgcolor  = "#282828";
static const char* normfgcolor  = "#c2c2b0";
static const char* selbgcolor   = "#222222";
static const char* selfgcolor   = "#5f8787";
static const char* urgbgcolor   = "#5f8787";
static const char* urgfgcolor   = "#222222";
