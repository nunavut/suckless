/* Cheri theme */
static const char* normbgcolor  = "#070811";
static const char* normfgcolor  = "#c4cddb";
static const char* selbgcolor   = "#0a0c18";
static const char* selfgcolor   = "#c4cddb";
static const char* urgbgcolor   = "#070811";
static const char* urgfgcolor   = "#345388";
