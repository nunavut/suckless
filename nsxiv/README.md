Custom nsxiv

# Used patches:
- random-image.patch

Sadly, nsxiv is under the tyrannical General Public License version 3. So everything here
is under GPL to.
