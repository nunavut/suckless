static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#1c1213" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#a87569" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#3a2425" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#a87569" };
#endif
