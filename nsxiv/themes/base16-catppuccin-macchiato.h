static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#24273a" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#b7bdf8" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#363a4f" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#cad3f5" };
#endif
