static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#181818" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#e8e8e8" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#282828" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#d8d8d8" };
#endif
