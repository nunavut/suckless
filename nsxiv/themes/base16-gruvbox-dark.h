static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#1d2021" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#d5c4a1" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#3c3836" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#fbf1c7" };
#endif
