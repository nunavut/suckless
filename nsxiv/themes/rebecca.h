static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#292a44" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#f1eff8" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#383a62" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#a0a0c5" };
#endif
