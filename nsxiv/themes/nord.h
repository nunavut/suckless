static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#3b4252" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#d8dee9" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#2e3440" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#d8dee9" };
#endif
