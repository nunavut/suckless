static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#1e1e2e" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#cdd6f4" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#313244" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#89b4fa" };
#endif
