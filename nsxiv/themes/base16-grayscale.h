static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#101010" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#b9b9b9" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#202020" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#b9b9b9" };
#endif
