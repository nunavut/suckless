static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#070811" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#c4cddb" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#0a0c18" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#898f99" };
#endif
