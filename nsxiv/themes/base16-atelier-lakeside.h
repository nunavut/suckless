static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#161b1d" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#7ea2b4" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#1f292e" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#7ea2b4" };
#endif
