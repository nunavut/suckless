static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#1c2023" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#c7ccd1" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#565e65" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#f3f4f5" };
#endif
