static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#282c34" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#abb2bf" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#353b45" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#abb2bf" };
#endif
