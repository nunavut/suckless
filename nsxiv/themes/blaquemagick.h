static const char *WIN_BG[]   = { "Nsxiv.window.background",   "#222222" };
static const char *WIN_FG[]   = { "Nsxiv.window.foreground",   "#c2c2b0" };
static const char *MARK_FG[]  = { "Nsxiv.mark.foreground",      NULL };
#if HAVE_LIBFONTS
static const char *BAR_BG[]   = { "Nsxiv.bar.background",       "#5f8787" };
static const char *BAR_FG[]   = { "Nsxiv.bar.foreground",       "#222222" };
#endif
