static void quitprompt(const Arg *arg);
void
quitprompt(const Arg *arg)
{
	FILE *pp = popen("printf 'no\\nrestart\\nyes' | dmenu -i -l 0 -p 'quit dwm?'", "r");
	if(pp != NULL) {
		char buf[1024];
		if (fgets(buf, sizeof(buf), pp) == NULL) {
			fprintf(stderr, "Quitprompt: Error reading pipe!\n");
			return;
		}
		if (strcmp(buf, "yes\n") == 0) {
			pclose(pp);
			restart = 0;
			quit(NULL);
		} else if (strcmp(buf, "restart\n") == 0) {
			pclose(pp);
			restart = 1;
			quit(NULL);
		} else if (strcmp(buf, "no\n") == 0) {
			pclose(pp);
			return;
		}
	}
}
