/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx = 2;
static const unsigned int snap     = 32;
static const int showbar           = 1;
static const int topbar            = 1;
static const char *fonts[]         = { "JetBrainsMono Nerd Font:size=7" };
static const int focusonwheel      = 1;
static const unsigned int gappih   = 10;
static const unsigned int gappiv   = 10;
static const unsigned int gappoh   = 20;
static const unsigned int gappov   = 20;
static       int smartgaps         = 0;
static const int horizpadbar       = -2; 
static const int vertpadbar        = 9;
static const double activeopacity  = 1.0f;
static const double inactiveopacity = 0.8f;

/* theme */
#include "themes/nord.h"

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   focusopacity    unfocusopacity     monitor */
	{ "gimp",     NULL,       NULL,       0,            1,           1.0,            1.0,               -1 },
	{ "herbe",    NULL,       NULL,       0,            0,           0.8,            0.8,               -1 },
	{ NULL,       "herbe",    NULL,       0,            0,           0.8,            0.8,               -1 },
	{ "tabbed",   NULL,       NULL,       0,            0,           0.9,            0.7,               -1 },
	{ "st",       NULL,       NULL,       0,            0,           0.9,            0.7,               -1 },
};

/* layout(s) */
static const float mfact     = 0.55;
static const int nmaster     = 1;
static const int resizehints = 0;
static const int lockfullscreen = 0;

/* includes */
#define FORCE_VSPLIT 1
#include "vanitygaps.c"
#include "shift-tools.c"
#include "movestack.c"
#include "selfrestart.c"
#include "quitprompt.c"
#include "focusurgent.c"
#include "X11/XF86keysym.h"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },
	{ "[M]",      monocle },
	{ "[@]",      spiral },
	{ "[\\]",     dwindle },
	{ "H[]",      deck },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
	{ "HHH",      grid },
	{ "###",      nrowgrid },
	{ "---",      horizgrid },
	{ ":::",      gaplessgrid },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "><>",      NULL },
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *term[]       = { "st", NULL };
static const char *tabbed[]     = { "tabbed", "-c", "st", "-w", NULL };
static const char *dmenu[]      = { "dmenu_run", NULL };
static const char *nnn[]        = { "st", "-e", "tmux", "new-session", "-c", "/dev/null", "nnn", NULL };
static const char *dmenu_pass[] = { "dmenu-pass", NULL };
static const char *bookmarks[]  = { "bk", "-d", "-l", NULL };
static const char *bookmarks_c[] = { "bk", "-d", "-c", NULL };
static const char *bookmarks_r[] = { "bk", "-d", "-r", NULL };
static const char *dmenu_xrandr_b[] = { "dmenu-xrandr", "-b", NULL };
static const char *dmenu_xrandr_r[] = { "dmenu-xrandr", "-r", NULL };
static const char *dmenu_xrandr_e[] = { "dmenu-xrandr", "-R", NULL };
static const char *rssadd[]     = { "rssadd", "-c", NULL };
static const char *shot[]       = { "dmenu-shot", NULL};
static const char *voli[]       = { "volume-handler", "-i", NULL};
static const char *vold[]       = { "volume-handler", "-d", NULL};
static const char *volm[]       = { "volume-handler", "-m", NULL};

static const Key keys[] = {
	/* modifier                     key        function        argument */
    /* spawn functions */
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenu } },
	{ MODKEY|ShiftMask,             XK_p,      spawn,          {.v = dmenu_pass } },
	{ MODKEY|ShiftMask,             XK_b,      spawn,          {.v = bookmarks } },
	{ MODKEY|Mod1Mask,              XK_b,      spawn,          {.v = bookmarks_c } },
	{ MODKEY|ControlMask,           XK_b,      spawn,          {.v = bookmarks_r } },
	{ MODKEY|ShiftMask,             XK_x,      spawn,          {.v = dmenu_xrandr_r } },
	{ MODKEY|ShiftMask,             XK_n,      spawn,          {.v = dmenu_xrandr_e } },
	{ MODKEY|ControlMask,           XK_n,      spawn,          {.v = dmenu_xrandr_b } },
	{ MODKEY,                       XK_r,      spawn,          {.v = rssadd } },
	{ MODKEY,                       XK_Print,  spawn,          {.v = shot } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = term } },
	{ MODKEY|Mod1Mask,              XK_Return, spawn,          {.v = tabbed } },
	{ MODKEY|ShiftMask,             XK_z,      spawn,          {.v = nnn } },

    /* volume control */
	{ MODKEY,                       XK_minus,  spawn,          {.v = vold } },
	{ MODKEY,                       XK_equal,  spawn,          {.v = voli } },

	{ MODKEY,                       XF86XK_AudioLowerVolume, spawn, {.v = vold } },
	{ MODKEY,                       XF86XK_AudioRaiseVolume, spawn, {.v = voli } },
        { MODKEY,                       XF86XK_AudioMute, spawn,   {.v = volm } },

    /* window stuff */
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_o,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_h,      setcfact,       {.f = -0.25} },
	{ MODKEY|ShiftMask,             XK_l,      setcfact,       {.f = +0.25} },
	{ MODKEY|ShiftMask,             XK_o,      setcfact,       {.f =  0.00} },
	{ MODKEY|Mod1Mask,              XK_u,      incrgaps,       {.i = -1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_u,      incrgaps,       {.i = +1 } },
	{ MODKEY|Mod1Mask,              XK_i,      incrigaps,      {.i = -1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_i,      incrigaps,      {.i = +1 } },
	{ MODKEY|Mod1Mask,              XK_o,      incrogaps,      {.i = -1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_o,      incrogaps,      {.i = +1 } },
	{ MODKEY|Mod1Mask,              XK_6,      incrihgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask,              XK_7,      incrivgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask,              XK_8,      incrohgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask,              XK_9,      incrovgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask,              XK_0,      togglegaps,     {0} },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
        { MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },

    /* layouts */
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[13]} },

	{ MODKEY|ShiftMask,             XK_f,      setlayout,      {.v = &layouts[2]} },
        { MODKEY|ShiftMask,             XK_d,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,             XK_s,      setlayout,      {.v = &layouts[4]} },
	{ MODKEY|ShiftMask,             XK_g,      setlayout,      {.v = &layouts[10]} },

    /* moveresize */
    { MODKEY,                       XK_Down,   moveresize,     {.v = "0x 25y 0w 0h" } },
    { MODKEY,                       XK_Up,     moveresize,     {.v = "0x -25y 0w 0h" } },
    { MODKEY,                       XK_Right,  moveresize,     {.v = "25x 0y 0w 0h" } },
    { MODKEY,                       XK_Left,   moveresize,     {.v = "-25x 0y 0w 0h" } },
    { MODKEY|ShiftMask,             XK_Down,   moveresize,     {.v = "0x 0y 0w 25h" } },
    { MODKEY|ShiftMask,             XK_Up,     moveresize,     {.v = "0x 0y 0w -25h" } },
    { MODKEY|ShiftMask,             XK_Right,  moveresize,     {.v = "0x 0y 25w 0h" } },
    { MODKEY|ShiftMask,             XK_Left,   moveresize,     {.v = "0x 0y -25w 0h" } },

    /* clientopacity */
    { MODKEY|ShiftMask,             XK_bracketleft,      changefocusopacity,   {.f = +0.025}},
    { MODKEY|ShiftMask,             XK_bracketright,     changefocusopacity,   {.f = -0.025}},
    { MODKEY|ControlMask|ShiftMask, XK_bracketleft,      changeunfocusopacity, {.f = +0.025}},
    { MODKEY|ControlMask|ShiftMask, XK_bracketright,     changeunfocusopacity, {.f = -0.025}},

    /* shift tools */
	{ ControlMask|Mod1Mask,         XK_Right,  shiftview,      { .i = +1 } },
	{ ControlMask|Mod1Mask,         XK_Left,   shiftview,      { .i = -1 } },
	{ MODKEY|Mod1Mask,              XK_j,      shiftview,      { .i = +1 } },
	{ MODKEY|Mod1Mask,              XK_k,      shiftview,      { .i = -1 } },
	{ MODKEY|Mod1Mask,              XK_l,      shiftview,      { .i = +1 } },
	{ MODKEY|Mod1Mask,              XK_h,      shiftview,      { .i = -1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_j,      shifttag,       { .i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_k,      shifttag,       { .i = -1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_l,      shifttag,       { .i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_h,      shifttag,       { .i = -1 } },

    /* misc */
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
        { MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY|ShiftMask,             XK_r,      self_restart,   {0} },
	{ Mod1Mask,                     XK_F11,    togglefullscr,  {0} },
	{ Mod1Mask,                     XK_Return, togglefullscr,  {0} },
	{ MODKEY|ShiftMask,             XK_q,      quitprompt,     {0} },
	{ MODKEY,                       XK_u,      focusurgent,    {0} },

    /* tagkeys */    
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = term } },
        { ClkStatusText,        0,              Button4,        shiftview,      { .i = -1 } },
	{ ClkStatusText,        0,              Button5,        shiftview,      { .i = +1 } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            0,              Button4,        shiftview,      { .i = -1 } },
	{ ClkTagBar,            0,              Button5,        shiftview,      { .i = +1 } },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

