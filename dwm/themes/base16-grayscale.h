static char normbgcolor[]           = "#101010";
static char normfgcolor[]           = "#b9b9b9";
static char normbordercolor[]       = "#101010";
static char selbgcolor[]            = "#252525";
static char selfgcolor[]            = "#f7f7f7";
static char selbordercolor[]        = "#525252";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
