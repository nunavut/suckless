static char normbgcolor[]           = "#222222";
static char normfgcolor[]           = "#c2c2b0";
static char normbordercolor[]       = "#222222";
static char selbgcolor[]            = "#666666";
static char selfgcolor[]            = "#c2c2b0";
static char selbordercolor[]        = "#666666";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
