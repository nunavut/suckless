static char normbgcolor[]           = "#000000";
static char normfgcolor[]           = "#c1c1c1";
static char normbordercolor[]       = "#000000";
static char selbgcolor[]            = "#111111";
static char selfgcolor[]            = "#c1c1c1";
static char selbordercolor[]        = "#101010";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
