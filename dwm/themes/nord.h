static char normbgcolor[]           = "#2e3440";
static char normbordercolor[]       = "#2e3440";
static char normfgcolor[]           = "#d8dee9";
static char selbordercolor[]        = "#5e81ac";
static char selbgcolor[]            = "#3b4252";
static char selfgcolor[]            = "#e5e9f0";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
