static char normbgcolor[]           = "#1c1213";
static char normfgcolor[]           = "#a87569";
static char normbordercolor[]       = "#1c1213";
static char selbgcolor[]            = "#3a2425";
static char selfgcolor[]            = "#a87569";
static char selbordercolor[]        = "#3a2425";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
