static char normbgcolor[]           = "#282c34";
static char normfgcolor[]           = "#abb2bf";
static char normbordercolor[]       = "#545862";
static char selbgcolor[]            = "#353b45";
static char selfgcolor[]            = "#c8ccd4";
static char selbordercolor[]        = "#545862";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
