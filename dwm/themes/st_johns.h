static char normbgcolor[]           = "#101111";
static char normfgcolor[]           = "#d5c1ab";
static char normbordercolor[]       = "#101111";
static char selbgcolor[]            = "#027571";
static char selfgcolor[]            = "#d5c1ab";
static char selbordercolor[]        = "#9c642a";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
