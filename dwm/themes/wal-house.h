/* random wal theme */

static const char norm_fg[] = "#bea392";
static const char norm_bg[] = "#151716";
static const char norm_border[] = "#857266";

static const char sel_fg[] = "#bea392";
static const char sel_bg[] = "#443E41";
static const char sel_border[] = "#bea392";

static const char urg_fg[] = "#bea392";
static const char urg_bg[] = "#353D42";
static const char urg_border[] = "#353D42";

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
    /* [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border }, */
};
