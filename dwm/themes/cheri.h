static char normbgcolor[]           = "#070811";
static char normfgcolor[]           = "#7987a2";
static char normbordercolor[]       = "#070811";
static char selbgcolor[]            = "#181f31";
static char selfgcolor[]            = "#b7c6dd";
static char selbordercolor[]        = "#345388";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
