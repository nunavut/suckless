static char normbgcolor[]           = "#181818";
static char normfgcolor[]           = "#d8d8d8";
static char normbordercolor[]       = "#383838";
static char selbgcolor[]            = "#282828";
static char selfgcolor[]            = "#f8f8f8";
static char selbordercolor[]        = "#282828";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
