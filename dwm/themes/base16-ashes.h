static char normbgcolor[]           = "#1c2023";
static char normfgcolor[]           = "#c7ccd1";
static char normbordercolor[]       = "#565e65";
static char selbgcolor[]            = "#393f45";
static char selfgcolor[]            = "#f3f4f5";
static char selbordercolor[]        = "#383f45";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
