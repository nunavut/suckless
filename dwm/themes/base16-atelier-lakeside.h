static char normbgcolor[]           = "#161b1d";
static char normfgcolor[]           = "#7ea2b4";
static char normbordercolor[]       = "#161b1d";
static char selbgcolor[]            = "#1f292e";
static char selfgcolor[]            = "#ebf8ff";
static char selbordercolor[]        = "#1f292e";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
