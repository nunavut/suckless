static char normbgcolor[]           = "#313244";
static char normbordercolor[]       = "#313244";
static char normfgcolor[]           = "#bac2de";
static char selbgcolor[]            = "#1e1e2e";
static char selfgcolor[]            = "#cdd6f4";
static char selbordercolor[]        = "#f38ba8";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
