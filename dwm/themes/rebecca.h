static char normbgcolor[]           = "#292a44";
static char normfgcolor[]           = "#a0a0c5";
static char normbordercolor[]       = "#292a44";
static char selbgcolor[]            = "#383a62";
static char selfgcolor[]            = "#f1eff8";
static char selbordercolor[]        = "#b45bcf";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
