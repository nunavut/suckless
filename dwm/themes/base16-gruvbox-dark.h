static char normbgcolor[]           = "#1d2021";
static char normbordercolor[]       = "#3c3836";
static char normfgcolor[]           = "#d5c4a1";
static char selbordercolor[]        = "#3c3836";
static char selbgcolor[]            = "#3c3836";
static char selfgcolor[]            = "#fbf1c7";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
