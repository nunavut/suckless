static char normbgcolor[]           = "#24273a";
static char normfgcolor[]           = "#cad3f5";
static char normbordercolor[]       = "#363a4f";
static char selbgcolor[]            = "#1e2030";
static char selfgcolor[]            = "#c6a0f6";
static char selbordercolor[]        = "#c6a0f6";
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
