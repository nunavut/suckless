static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#c1c1c1", "#000000" },
	[SchemeSel] = { "#c1c1c1", "#111111" },
	[SchemeOut] = { "#c1c1c1", "#111111" },
	[SchemeSelHighlight] = { "#5f8787", "#111111" },
	[SchemeNormHighlight] = { "#c1c1c1", "#000000" },
};
