static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#cad3f5", "#24273a" },
	[SchemeSel] = { "#b7bdf8", "#1e2030" },
	[SchemeOut] = { "#a87569", "#1c1213" },
	[SchemeSelHighlight] = { "#c6a0f6", "#24273a" },
	[SchemeNormHighlight] = { "#c6a0f6", "#1e2030" },
};
