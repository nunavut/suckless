static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#d5c1ab", "#101111" },
	[SchemeSel] = { "#d5c1ab", "#027571" },
	[SchemeOut] = { "#d5c1ab", "#101111" },
	[SchemeSelHighlight] = { "#101111", "#027571" },
	[SchemeNormHighlight] = { "#027571", "#101111" },
};
