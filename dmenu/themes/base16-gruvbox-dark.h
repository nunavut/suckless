/* gruvbox (dark) theme */
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#d5c4a1", "#1d2021" },
	[SchemeSel] = { "#fbf1c7", "#32302f" },
	[SchemeOut] = { "#dddddd", "#000000" },
	[SchemeSelHighlight] = { "#b8bb26", "#1d2021" },
	[SchemeNormHighlight] = { "#b8bb26", "#1d2021" },
};
