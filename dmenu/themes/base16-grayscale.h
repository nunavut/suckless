static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#f7f7f7", "#101010" },
	[SchemeSel] = { "#f7f7f7", "#252525" },
	[SchemeOut] = { "#f7f7f7", "#383a62" },
	[SchemeSelHighlight] = { "#b9b9b9", "#252525" },
	[SchemeNormHighlight] = { "#b9b9b9", "#101010" },
};
