static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#a87569", "#1c1213" },
	[SchemeSel] = { "#a87569", "#3a2425" },
	[SchemeOut] = { "#a87569", "#1c1213" },
	[SchemeSelHighlight] = { "#c58c7b", "#6d4745" },
	[SchemeNormHighlight] = { "#a87569", "#1c1213" },
};
