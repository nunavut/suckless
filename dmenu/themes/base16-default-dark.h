static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#d8d8d8", "#181818" },
	[SchemeSel] = { "#e8e8e8", "#282828" },
	[SchemeOut] = { "#a87569", "#1c1213" },
	[SchemeSelHighlight] = { "#a1b56c", "#282828" },
	[SchemeNormHighlight] = { "#a1b56c", "#181818" },
};
