static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#abb2bf", "#282c34" },
	[SchemeSel] = { "#c8ccd4", "#353b45" },
	[SchemeOut] = { "#a87569", "#1c1213" },
	[SchemeSelHighlight] = { "#98c379", "#353b45" },
	[SchemeNormHighlight] = { "#98c379", "#282c34" },
};
