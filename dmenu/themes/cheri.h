static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#c4cddb", "#070811" },
	[SchemeSel] = { "#c4cddb", "#181f31" },
	[SchemeOut] = { "#c4cddb", "#070811" },
	[SchemeSelHighlight] = { "#898f99", "#181f31" },
	[SchemeNormHighlight] = { "#c4cddb", "#070811" },
};
