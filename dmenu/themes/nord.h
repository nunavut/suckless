/* nord (dark) theme */
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#d8dee9", "#2e3440" },
	[SchemeSel] = { "#d8dee9", "#3b4252" },
	[SchemeOut] = { "#dddddd", "#000000" },
	[SchemeMid] = { "#eeeeee", "#770000" },
	[SchemeSelHighlight] = { "#a3be8c", "#3b4252" },
	[SchemeNormHighlight] = { "#a3be8c", "#3b4252" },
};
