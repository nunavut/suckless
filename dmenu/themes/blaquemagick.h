static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#c2c2b0", "#222222" },
	[SchemeSel] = { "#5f8787", "#282828" },
	[SchemeOut] = { "#dddddd", "#000000" },
	[SchemeSelHighlight] = { "#5f8787", "#282828" },
	[SchemeNormHighlight] = { "#5f8787", "#222222" },
};
