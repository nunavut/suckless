/* gruvbox (dark) theme */
static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#a89984", "#282828" },
	[SchemeSel] = { "#fbf1c7", "#32302f" },
	[SchemeOut] = { "#dddddd", "#000000" },
	[SchemeSelHighlight] = { "#98971a", "#282828" },
	[SchemeNormHighlight] = { "#98971a", "#282828" },
};
