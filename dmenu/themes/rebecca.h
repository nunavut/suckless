static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#f1eff8", "#292a44" },
	[SchemeSel] = { "#f1eff8", "#663399" },
	[SchemeOut] = { "#f1eff8", "#383a62" },
	[SchemeSelHighlight] = { "#ccccff", "#663399" },
	[SchemeNormHighlight] = { "#ccccff", "#292a44" },
};
