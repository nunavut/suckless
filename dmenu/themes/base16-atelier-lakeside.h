static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#c1e4f6", "#161b1d" },
	[SchemeSel] = { "#ebf8ff", "#1f292e" },
	[SchemeOut] = { "#ebf8ff", "#516d7b" },
	[SchemeSelHighlight] = { "#7ea2b4", "#1f292e" },
	[SchemeNormHighlight] = { "#7ea2b4", "#161b1d" },
};
