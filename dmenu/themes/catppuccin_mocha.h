static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#cdd6f4", "#1e1e2e" },
	[SchemeSel] = { "#bac2de", "#313244" },
	[SchemeOut] = { "#dddddd", "#000000" },
	[SchemeSelHighlight] = { "#a6e3a1", "#313244" },
	[SchemeNormHighlight] = { "#a6e3a1", "#1e1e2e" },
};
