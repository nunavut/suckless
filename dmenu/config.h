/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy  = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"JetBrainsMono Nerd Font:size=8"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const unsigned int alpha = 0xfa;     /* Amount of opacity. 0xff is opaque */
static const int user_bh = 4;               /* add an defined amount of pixels to the bar height */

static const unsigned int alphas[SchemeLast][2] = {
	[SchemeNorm] = { 0xff, 0xfa },
	[SchemeSel] = { 0xff, 0xfa },
	[SchemeOut] = { 0xff, 0xfa },
};

/* theme */
#include "themes/nord.h"

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 6;
static unsigned int columns    = 1;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
