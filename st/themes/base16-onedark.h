/* onedark */
const char *colorname[] = {
  [0] = "#282c34",
  [1] = "#e06",
  [2] = "#98c379",
  [3] = "#e5c07b",
  [4] = "#61afef",
  [5] = "#c678dd",
  [6] = "#56b6c2",
  [7] = "#abb2bf",
  [8] = "#545862",
  [9] = "#e06c75",
  [10] = "#98c379",
  [11] = "#e5c07b",
  [12] = "#61afef",
  [13] = "#c678dd",
  [14] = "#56b6c2",
  [15] = "#c8ccd4",
  /* special colors */
  [256] = "#282c34", /* background */
  [257] = "#abb2bf", /* foreground */
  [258] = "#abb2bf",     /* cursor */
};
/* Default colors (colorname index)
 * foreground, background, cursor */
unsigned int defaultbg = 0;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
