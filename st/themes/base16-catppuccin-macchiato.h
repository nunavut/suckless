/* ashes */
const char *colorname[] = {
  [0] = "#24273a",
  [1] = "#ed8796",
  [2] = "#a6da95",
  [3] = "#eed49f",
  [4] = "#8aadf4",
  [5] = "#c6a0f6",
  [6] = "#8bd5ca",
  [7] = "#cad3f5",
  [8] = "#363a4f",
  [9] = "#ed8796",
  [10] = "#98c379",
  [11] = "#eed49f",
  [12] = "#8aadf4",
  [13] = "#c6a0f6",
  [14] = "#8bd5ca",
  [15] = "#b7bdf8",
  /* special colors */
  [256] = "#24273a", /* background */
  [257] = "#cad3f5", /* foreground */
  [258] = "#cad3f5",     /* cursor */
};
/* Default colors (colorname index)
 * foreground, background, cursor */
unsigned int defaultbg = 0;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
