/* random pywal theme */
const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#151716", /* black   */
  [1] = "#353D42", /* red     */
  [2] = "#443E41", /* green   */
  [3] = "#39464A", /* yellow  */
  [4] = "#3E5861", /* blue    */
  [5] = "#4B4E4D", /* magenta */
  [6] = "#5F605F", /* cyan    */
  [7] = "#bea392", /* white   */

  /* 8 bright colors */
  [8]  = "#857266",  /* black   */
  [9]  = "#353D42",  /* red     */
  [10] = "#443E41", /* green   */
  [11] = "#39464A", /* yellow  */
  [12] = "#3E5861", /* blue    */
  [13] = "#4B4E4D", /* magenta */
  [14] = "#5F605F", /* cyan    */
  [15] = "#bea392", /* white   */

  /* special colors */
  [256] = "#151716", /* background */
  [257] = "#bea392", /* foreground */
  [258] = "#bea392",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
