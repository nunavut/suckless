const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#070811", /* black   */
  [1] = "#A17360", /* red     */
  [2] = "#7987a2", /* green   */
  [3] = "#62708C", /* yellow  */
  [4] = "#345388", /* blue    */
  [5] = "#7C95C1", /* magenta */
  [6] = "#8D98AE", /* cyan    */
  [7] = "#c4cddb", /* white   */

  /* 8 bright colors */
  [8]  = "#898f99",  /* black   */
  [9]  = "#A17360",  /* red     */
  [10] = "#92814b", /* green   */
  [11] = "#ae9b55", /* yellow  */
  [12] = "#345388", /* blue    */
  [13] = "#b767a1", /* magenta */
  [14] = "#95bded", /* cyan    */
  [15] = "#c4cddb", /* white   */

  /* misc
  #0a0c18
  #181f31 */

  /* special colors */
  [256] = "#070811", /* background */
  [257] = "#c4cddb", /* foreground */
  [258] = "#c4cddb",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
