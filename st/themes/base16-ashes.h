/* ashes */
const char *colorname[] = {
  [0] = "#1c2023",
  [1] = "#c7ae95",
  [2] = "#95c7ae",
  [3] = "#aec795",
  [4] = "#ae95c7",
  [5] = "#c795ae",
  [6] = "#95aec7",
  [7] = "#c7ccd1",
  [8] = "#565e65",
  [9] = "#c7ae95",
  [10] = "#98c379",
  [11] = "#aec795",
  [12] = "#ae95c7",
  [13] = "#c795ae",
  [14] = "#95aec7",
  [15] = "#f3f4f5",
  /* special colors */
  [256] = "#1c2023", /* background */
  [257] = "#c7ccd1", /* foreground */
  [258] = "#c7ccd1",     /* cursor */
};
/* Default colors (colorname index)
 * foreground, background, cursor */
unsigned int defaultbg = 0;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
