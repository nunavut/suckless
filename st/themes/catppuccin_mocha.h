/* Catppuccin Mocha theme */
static const char *colorname[] = {
	/* 8 normal colors */
    "#45475a", /* black   */
    "#f38ba8", /* red     */
    "#a6e3a1", /* green   */
    "#f9e2af", /* yellow  */
    "#89b4fa", /* blue    */
    "#f5c2e7", /* magenta */
    "#94e2d5", /* cyan    */
    "#bac2de", /* white   */

    "#585b70", /* black   */
    "#f38ba8", /* red     */
    "#a6e3a1", /* green   */
    "#f9e2af", /* yellow  */
    "#89b4fa", /* blue    */
    "#f5c2e7", /* magenta */
    "#94e2d5", /* cyan    */
    "#a6adc8", /* white   */

    [255] = 0,

    "#cdd6f4", /* default foreground colour */
    "#1e1e2e", /* default background colour */
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 256;
unsigned int defaultbg = 257;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
