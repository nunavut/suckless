/* apprentice */
const char *colorname[] = {
  [0] = "#262626",
  [1] = "#444444",
  [2] = "#ffffaf",
  [3] = "#87af87",
  [4] = "#8787af",
  [5] = "#5fafaf",
  [6] = "#87afd7",
  [7] = "#5f5f87",
  [8] = "#5f875f",
  [9] = "#444444",
  [10] = "#98c379",
  [11] = "#87af87",
  [12] = "#8787af",
  [13] = "#5fafaf",
  [14] = "#87afd7",
  [15] = "#6c6c6c",
  /* special colors */
  [256] = "#262626", /* background */
  [257] = "#5f5f87", /* foreground */
  [258] = "#5f5f87",     /* cursor */
};
/* Default colors (colorname index)
 * foreground, background, cursor */
unsigned int defaultbg = 0;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
