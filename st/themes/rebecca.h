const char *colorname[] = {
  /* Rebecca (modified)
   * based on https://github.com/vic/base16-rebecca */

  /* 8 normal colors */
  [0] = "#292a44", /* black   */
  [1] = "#ff79c6", /* red     */
  [2] = "#2de0a7", /* green   */
  [3] = "#f0eda1", /* yellow  */
  [4] = "#7aa5ff", /* blue    */
  [5] = "#ae81ff", /* magenta */
  [6] = "#8eaee0", /* cyan    */
  [7] = "#a0a0c5", /* white   */

  /* 8 bright colors */
  [8]  = "#53495d",  /* black   */
  [9]  = "#ff94c4",  /* red     */
  [10] = "#2de0a7", /* green   */
  [11] = "#f0eda1", /* yellow  */
  [12] = "#7aa5ff", /* blue    */
  [13] = "#978ee0", /* magenta */
  [14] = "#8eaee0", /* cyan    */
  [15] = "#f1eff8", /* white   */

  /* special colors */
  [256] = "#292a44", /* background */
  [257] = "#f1eff8", /* foreground */
  [258] = "#f1eff8",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
