const char *colorname[] = {
  /* base16-caroline */
  [0] = "#1c1213",
  [1] = "#c24f57",
  [2] = "#806c61",
  [3] = "#f28171",
  [4] = "#684c59",
  [5] = "#a63650",
  [6] = "#6b6566",
  [7] = "#a87569",
  [8] = "#6d4745",
  [9] = "#c24f57",
  [10] = "#806c61",
  [11] = "#f28171",
  [12] = "#684c59",
  [13] = "#a63650",
  [14] = "#6b6566",
  [15] = "#e3a68c",

  /* special colors */
  [256] = "#1c1213", /* background */
  [257] = "#a87569", /* foreground */
  [258] = "#a87569",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
