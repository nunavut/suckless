const char *colorname[] = {
  /* base16-grayscale */

  /* 8 normal colors */
  [0] = "#161b1d", /* black   */
  [1] = "#d22d72", /* red     */
  [2] = "#568c3b", /* green   */
  [3] = "#8a8a0f", /* yellow  */
  [4] = "#257fad", /* blue    */
  [5] = "#6b6bb8", /* magenta */
  [6] = "#2d8f6f", /* cyan    */
  [7] = "#7ea2b4", /* white   */

  /* 8 bright colors */
  [8]  = "#525252",  /* black   */
  [9]  = "#d22d72",  /* red     */
  [10] = "#568c3b", /* green   */
  [11] = "#8a8a0f", /* yellow  */
  [12] = "#257fad", /* blue    */
  [13] = "#6b6bb8", /* magenta */
  [14] = "#2d8f6f", /* cyan    */
  [15] = "#ebf8ff", /* white   */

  /* special colors */
  [256] = "#161b1d", /* background */
  [257] = "#7ea2b4", /* foreground */
  [258] = "#7ea2b4",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
