const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#101111", /* black   */
  [1] = "#e65843", /* red     */
  [2] = "#6A635F", /* green   */
  [3] = "#9C642A", /* yellow  */
  [4] = "#027571", /* blue    */
  [5] = "#957359", /* magenta */
  [6] = "#c3ecfb", /* cyan    */
  [7] = "#d5c1ab", /* white   */

  /* 8 bright colors */
  [8]  = "#958777",  /* black   */
  [9]  = "#e65843",  /* red     */
  [10] = "#6A635F", /* green   */
  [11] = "#9C642A", /* yellow  */
  [12] = "#027571", /* blue    */
  [13] = "#957359", /* magenta */
  [14] = "#c3ecfb", /* cyan    */
  [15] = "#d5c1ab", /* white   */

  /* special colors */
  [256] = "#101111", /* background */
  [257] = "#d5c1ab", /* foreground */
  [258] = "#d5c1ab",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
