/* default-dark */
const char *colorname[] = {
  [0] = "#181818",
  [1] = "#ab4642",
  [2] = "#a1b56c",
  [3] = "#f7ca88",
  [4] = "#7cafc2",
  [5] = "#ba8baf",
  [6] = "#86c1b9",
  [7] = "#d8d8d8",
  [8] = "#383838",
  [9] = "#ab4642",
  [10] = "#98c379",
  [11] = "#f7ca88",
  [12] = "#7cafc2",
  [13] = "#ba8baf",
  [14] = "#86c1b9",
  [15] = "#f8f8f8",
  /* special colors */
  [256] = "#282c34", /* background */
  [257] = "#d8d8d8", /* foreground */
  [258] = "#d8d8d8",     /* cursor */
};
/* Default colors (colorname index)
 * foreground, background, cursor */
unsigned int defaultbg = 0;
unsigned int defaultfg = 257;
unsigned int defaultcs = 258;
unsigned int defaultrcs= 258;
