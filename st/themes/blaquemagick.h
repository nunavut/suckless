/* Blaquemagick theme */
static const char *colorname[] = {
	/* 8 normal colors */
    "#222222", /* black   */
    "#5f8787", /* red     */
    "#666666", /* green   */
    "#87875f", /* yellow  */
    "#888888", /* blue    */
    "#8181a6", /* magenta */
    "#777777", /* cyan    */
    "#bac2de", /* white   */

    "#585b70", /* black   */
    "#f38ba8", /* red     */
    "#666666", /* green   */
    "#87875f", /* yellow  */
    "#888888", /* blue    */
    "#8181a6", /* magenta */
    "#777777", /* cyan    */
    "#c1c1c1", /* white   */

    [255] = 0,

    "#c2c2b0", /* default foreground colour */
    "#222222", /* default background colour */
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 256;
unsigned int defaultbg = 257;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;
